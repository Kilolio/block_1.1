package com.company;
import java.util.Scanner;
import static java.lang.Math.*;

public class Main {
    public static void main(String[] args)  {

        double i=1;
        while (i!=0){
            Scanner sc= new Scanner(System.in);
            System.out.println("Введите номер задачи,которую хотите проверить , номера задач {4,5,6,7}, закончить проверку нажмите 0 ");
            double a = sc.nextDouble();
            if(a==4){KvadrUravn();}
            if(a==5){TabuSin();}
            if(a==6){LinUravn();}
            if(a==7){ExpTeilor();}
            i=a;
        }

    }
    static void KvadrUravn(){
        Scanner sc= new Scanner(System.in);
        System.out.println("Введите коэфиценты квадратного уравнения:");
        double a = sc.nextDouble();
        double b = sc.nextDouble();
        double c= sc.nextDouble();
        double D=b*b-4*a*c;
        if (D>0){
            double x1;
            double x2;
            x1=((-1)*b+Math.sqrt(D))/(2*a);
            x2=((-1)*b-Math.sqrt(D))/(2*a);
            System.out.println("Первый корень :"+ x1);
            System.out.println("Второй корень :"+ x2);
        }
        if(D==0){
            double x1=b*(-1)/(2*a);
            System.out.println("Один корень: "+ x1);

        }
        if(D<0){
            System.out.println("Вещественных корней нет");

        }

    }
    static void TabuSin(){
        System.out.println("Введите левую , правую границу табулирования sin() и шаг табуляции");
        Scanner in=new Scanner(System.in);
        double a = in.nextDouble();
        double b = in.nextDouble();
        double c = in.nextDouble();
        System.out.println();
        for(double i=a;i<b;i=i+c){

            System.out.println(Math.sin(i));
        }



    }
    static void LinUravn(){
        System.out.println("Введите коэфиценты 1-ого и 2-ого уравнения a1,b1,c1, a2,b2,c2 . Вид уравнения ax+by+c=0 ");
        Scanner in=new Scanner(System.in);
        double a1 = in.nextDouble();
        double b1 = in.nextDouble();
        double c1 = in.nextDouble();
        double a2 = in.nextDouble();
        double b2 = in.nextDouble();
        double c2 = in.nextDouble();
        double x = 0;
        double y = 0;
        System.out.println();
        y=((a2*-b1)+b2*a1);
        y=((-1)*(a2*-c1+c2*a1))/y;
        x=(-b1*y+-c1)/a1;

        if (x*0+y*0!=0){ System.out.println("Нет решения"); } else{
            System.out.println(x);
            System.out.println(y);
        }






    }
    static void ExpTeilor(){
        System.out.println("Вычисление функции exp(x) разложением в ряд Тейлора с" +
                "заданной точностью. Введите x и точность a. ");
        Scanner in=new Scanner(System.in);
        double x=  in.nextDouble();
        double a=  in.nextDouble();
        double sum=1;
        double mod=a+1;
        double fact=1;
        for(int i=0;mod>=a;i++){
            for(int j=i;j!=0;j--){
                fact=fact*j;

            }
            mod=pow(x,i)/fact;
            sum+=mod;
        }
        System.out.println("Сумма ряда ="+sum);


    }


}
